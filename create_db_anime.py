#!/usr/bin/env python3

import json
from models import Characters, Shows, Voice_Actors, CharactersShows, db, app

def init_app(app):
    from models import db
    with app.app_context():
        db.create_all()

# ------------
# load_json
# ------------
def load_json(filename):
    """
    return a python dict jsn
    filename a json file
    """
    with open(filename) as file:
        jsn = json.load(file)
        file.close()

    return jsn

# ------------
# create_chars
# ------------
def create_chars():
    """
    populate characters table
    """
    
    characters = load_json('characters_final.json')

    # iterating through all of the characters
    for character in characters:
        id = character['mal_id']
        name = character['name']
        image = character['images']['jpg']['image_url']
        about = character['about']

        # some characters do not have voice actors
        try:
            voiced_by_id = character['voices']['person']['mal_id']
            voice = db.session.query(Voice_Actors).filter_by(id=voiced_by_id).first()
            voice_id = voice.id

        except:
            voice_id = None
        
        exists = db.session.query(Characters).filter_by(id = id).scalar() is not None

        if not exists:
            # Create new character object only if it doesn't exist
            new_char = Characters(id=id, name=name, image = image, voiced_by_id = voice_id, about = about)
            # Adding into the session. 
            db.session.add(new_char)
        
        # commit the session to my DB.
        db.session.commit()
        
# ------------
# create_shows
# ------------
def create_shows():
    """
    populate shows table
    """
    
    shows = load_json('shows.json')

    # iterating through all of the shows
    for show in shows:
        id = show['mal_id']
        image = show['images']['jpg']['large_image_url']
        title = show['title']
        episodes = show['episodes']
        rating = show['rating']
        status = show['status']
        
        exists = db.session.query(Shows).filter_by(id = id).scalar() is not None

        if not exists:
            # Create new show object only if it doesn't exist
            new_show = Shows(id=id, title=title, image = image, episodes = episodes, rating = rating, status = status)
            # Adding the show into the session. 
            db.session.add(new_show)
        
        # commit the session to my DB.
        db.session.commit()

# ------------
# create_voice_actors
# ------------
def create_voice_actors():
    """
    populate voice actor table
    """
    
    voices = load_json('voices_final.json')

    # iterating through all of the voice_actors
    for voice in voices:
        
        id = voice['mal_id']
        name = voice['name']
        birthday = voice['birthday']
        if birthday:
            birthday = birthday[:10]
        image = voice['images']['jpg']['image_url']
        about = voice['about']        
        
        exists = db.session.query(Voice_Actors).filter_by(id = id).scalar() is not None

        if not exists:
            # Create new voice_actor object only if it doesn't exist
            new_voice = Voice_Actors(id=id, name=name, birthday = birthday, image = image, about = about)
            # Adding actor into the session. 
            db.session.add(new_voice)
        
        # commit the session to my DB.
        db.session.commit()

# ------------
# create_intermediate table
# ------------
def create_characters_shows():
    """
    Create intermediate table entries for characters and shows, 
    assuming characters and shows are already populated, while
    preventing duplicate entries.
    """
    
    characters = load_json('characters_final.json')
    shows = load_json('shows.json')

    # relating each individual value in shows to a value in list anime for each of the characters
    # iterating through each show
    for show in shows:
        show_id = show['mal_id']
        show_object = Shows.query.filter_by(id=show_id).first()  # Get existing Shows object
        
        # iterating through each character
        for character in characters:
            char_animes = character['anime']

            # iterating through each anime the character is in and checking if the anime is in shows
            for anime in char_animes:
                if show_id == anime['anime']['mal_id']:
                    character_object = Characters.query.filter_by(id=character['mal_id']).first()  # Get existing Character object
                    
                    # Check if the relationship already exists:
                    if not db.session.query(CharactersShows).filter_by(character_id=character_object.id, show_id=show_object.id).first():
                        # Establish the relationship only if it doesn't already exist
                        character_object.charshows.append(show_object)    
                        # commit the session to my DB.
    
    db.session.commit()

db.drop_all()
db.create_all()

create_voice_actors()
create_chars()
create_shows()
create_characters_shows()
