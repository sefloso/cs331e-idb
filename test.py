import unittest
from flask import Flask
from models import CharactersShows, Shows, Characters, Voice_Actors, init_app, db
import coverage
class AnimeCharacterDBTestCases(unittest.TestCase):
    # ---------
    # insertion
    # ---------
    def setUp(self):
        app = Flask(__name__)
        app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///:memory:'
        app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
        init_app(app)
        self.app = app.test_client()
        with app.app_context():
            db.create_all()

    def tearDown(self):
        with self.app.application.app_context():
            db.drop_all()

    def test_character_insert_1(self):
        with self.app.application.app_context():
            char = Characters(
                id=10123456, 
                name='Chihiro', 
                image='chihiro.jpg' 
            )
            db.session.add(char)
            db.session.commit()

            r = Characters.query.get(10123456)
            self.assertEqual(r.id, 10123456)
            self.assertEqual(r.name, 'Chihiro')

    def test_character_insert_2(self):
        with self.app.application.app_context():
            char = Characters(
                id=101234567, 
                name='Naruto Uzumaki', 
                image='naruto.jpg' 
            )
            db.session.add(char)
            db.session.commit()

            r = Characters.query.get(101234567)
            self.assertEqual(r.id, 101234567)
            self.assertEqual(r.name, 'Naruto Uzumaki')

    def test_character_insert_3(self):
        with self.app.application.app_context():
            char = Characters(
                id=1012345678, 
                name='Ponyo', 
                image='ponyo.jpg' 
            )
            db.session.add(char)
            db.session.commit()

            r = Characters.query.get(1012345678)
            self.assertEqual(r.id, 1012345678)
            self.assertEqual(r.name, 'Ponyo')
            """
            # Cleanup
            db.session.delete(r)
            db.session.commit()
            """
    # ---------
    # relationships
    # ---------
    def test_character_show_relationship_1(self):
        """Tests the relationship between an anime character and the show Naruto Shippuden"""
        with self.app.application.app_context():
            show = Shows(title='Naruto Shippuden', image='naruto.jpg', episodes=500, rating='PG-13', status='Ongoing')
            char = Characters(name='Sasuke Uchiha', image='sasuke.jpg')
            db.session.add_all([show, char])
            db.session.commit()

            charactershows = CharactersShows(character_id=char.id, show_id=show.id)
            db.session.add(charactershows)
            db.session.commit()

            r = Characters.query.get(char.id)
            associated_shows = r.Shows 
            self.assertEqual(associated_shows[0].title, 'Naruto Shippuden')


    def test_character_show_relationship_2(self): 
        with self.app.application.app_context():
            show2 = Shows(title='Sanrio Show', image='sanrio.jpg', episodes=200, rating='PG-13', status='Ongoing')
            char2 = Characters(name='Hello Kitty', image='hk.jpg')
            db.session.add_all([show2, char2])
            db.session.commit()

            charactershows2 = CharactersShows(character_id=char2.id, show_id=show2.id)
            db.session.add(charactershows2)
            db.session.commit()

            r = Characters.query.get(char2.id)
            associated_shows2 = r.Shows 
            self.assertEqual(associated_shows2[0].title, 'Sanrio Show')


    def test_character_show_relationship_3(self):
        with self.app.application.app_context():
            show3 = Shows(title='Rilakkuma', image='bear.jpg', episodes=200, rating='PG-13', status='Ongoing')
            char3 = Characters(name='Korilakkuma', image='kk.jpg')
            db.session.add_all([show3, char3])
            db.session.commit()

            charactershows3 = CharactersShows(character_id=char3.id, show_id=show3.id)
            db.session.add(charactershows3)
            db.session.commit()

            r = Characters.query.get(char3.id)
            associated_shows3= r.Shows 
            self.assertEqual(associated_shows3[0].title, 'Rilakkuma')

    def test_character_voice_actor_relationship_1(self):
        """Tests the relationship between an anime character and a voice actor"""
        with self.app.application.app_context():
            va = Voice_Actors(name='Junko Takeuchi', image='junko.jpg')
            char = Characters(name="Izuku Midoriya", image='midoriya.jpg')
            char.voice_actor = va
            db.session.add_all([va, char])
            db.session.commit()

            r = Characters.query.filter_by(name="Izuku Midoriya").first()
            self.assertEqual(r.voice_actor.name, 'Junko Takeuchi')

    def test_character_voice_actor_relationship_2(self):
        
        with self.app.application.app_context():
            va = Voice_Actors(name='Brittany Spears', image='spears.jpg')
            char = Characters(name="AD", image='AD.jpg')
            char.voice_actor = va
            db.session.add_all([va, char])
            db.session.commit()

            r = Characters.query.filter_by(name="AD").first()  
            self.assertEqual(r.voice_actor.name, 'Brittany Spears') 

    def test_character_voice_actor_relationship_3(self):
            with self.app.application.app_context():
                va = Voice_Actors(name='Ian McKellen', image='IanK.jpg')
                char = Characters(name="Gandalf", image='gandalf.jpg')
                char.voice_actor = va
                db.session.add_all([va, char])
                db.session.commit()

                r = Characters.query.filter_by(name="Gandalf").first()  
                self.assertEqual(r.voice_actor.name, 'Ian McKellen') 
            """
            db.session.delete(r)
            db.session.commit()
            """
if __name__ == '__main__':
    init_app(Flask(__name__))  
    unittest.main()
