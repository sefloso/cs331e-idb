from flask import Flask, render_template, request, jsonify
import requests
from models import Shows, db, Characters, Voice_Actors, CharactersShows
from sqlalchemy import or_
from create_db_anime import app




@app.route('/shows/<int:show_id>')
def showShow(show_id):
    show = Shows.query.options(db.joinedload(Shows.charshows)).get_or_404(show_id)  
    characters = show.charshows 
    return render_template('showShow.html', show=show, characters=characters)


@app.route('/characters/<int:char_id>')
def showCharacter(char_id):
    character = Characters.query.options(db.joinedload(Characters.charshows), db.joinedload(Characters.voice)).get_or_404(char_id) 
    shows = list(character.charshows)
    voice_actor = character.voice
    return render_template('showCharacter.html', character=character, shows=shows, voice_actor=voice_actor)


@app.route('/voices/<int:va_id>')
def showVoice(va_id):
    voice_actor = Voice_Actors.query.options(db.joinedload(Voice_Actors.characters)).get_or_404(va_id)
    characters = voice_actor.characters
    return render_template('showVoice.html', voice_actor=voice_actor, characters=characters)

@app.route('/')
def splash(): 
    return render_template('index.html')

@app.route('/about')
def render_about():
    # get # of commits
    commit_api_url = 'https://gitlab.com/api/v4/projects/54360623/repository/commits'
    commit_response = requests.get(commit_api_url)
    if commit_response.status_code == 200:
        commit_data = commit_response.json()
        total_commits = len(commit_data)
    else:
        # if unsuccesfull api call, set to 0
        total_commits = 0

    issue_api_url = 'https://gitlab.com/api/v4/projects/54360623/issues'
    issue_response = requests.get(issue_api_url)

    if issue_response.status_code == 200:
        issue_data = issue_response.json()
        total_issues = len(issue_data)
    else:
        total_issues = 0 
    return render_template('about.html', total_commits=total_commits, total_issues=total_issues, total_tests=-999)



@app.route('/shows')
def render_shows():
    items_per_page = 6
    page = request.args.get('page', 1, type=int)

    total_shows = db.session.query(Shows).count()
    total_pages = (total_shows // items_per_page) + (total_shows % items_per_page > 0)
    
    sort_by = request.args.get('sort_by', 'id') 
    order = request.args.get('order', 'asc') 

    query = Shows.query
    if sort_by == 'episodes':
        query = query.order_by(Shows.episodes.asc() if order == 'asc' else Shows.episodes.desc())
    elif sort_by == 'rating':
        query = query.order_by(Shows.rating.asc() if order == 'asc' else Shows.rating.desc())
    elif sort_by == 'status':
        query = query.order_by(Shows.status.asc() if order == 'asc' else Shows.status.desc())

    total_shows = query.count() 
    total_pages = (total_shows // items_per_page) + (total_shows % items_per_page > 0)

    shows = query.offset((page - 1) * items_per_page).limit(items_per_page).all()

    return render_template('shows.html', shows=shows, page=page, total_pages=total_pages, order=order, sort_by=sort_by) 



@app.route('/characters')
def render_characters():
    items_per_page = 6
    page = request.args.get('page', 1, type=int)

    sort_by = request.args.get('sort_by', 'id')
    order = request.args.get('order', 'asc') 

    query = Characters.query.options(db.joinedload(Characters.charshows), db.joinedload(Characters.voice)) 
    if sort_by == 'name':
        query = query.order_by(Characters.name.asc() if order == 'asc' else Characters.name.desc())
    elif sort_by == 'voiced_by':  
        query = query.join(Characters.voice).order_by(Voice_Actors.name.asc() if order == 'asc' else Voice_Actors.name.desc())

    total_characters = query.count()
    total_pages = (total_characters // items_per_page) + (total_characters % items_per_page > 0)

    characters = query.offset((page - 1) * items_per_page).limit(items_per_page).all() 
    return render_template('characters.html', characters=characters, page=page, total_pages=total_pages, sort_by=sort_by, order=order) 



@app.route('/voices')   
def render_voice_actors():
    items_per_page = 6
    page = request.args.get('page', 1, type=int)

    sort_by = request.args.get('sort_by', 'id')  
    order = request.args.get('order', 'asc')
    
    query = Voice_Actors.query 
    if sort_by == 'name':
        query = query.order_by(Voice_Actors.name.asc() if order == 'asc' else Voice_Actors.name.desc())
    elif sort_by == 'birthday':
        query = query.order_by(Voice_Actors.birthday.asc() if order == 'asc' else Voice_Actors.birthday.desc())

    total_voices = db.session.query(Voice_Actors).count()
    total_pages = (total_voices // items_per_page) + (total_voices % items_per_page > 0)

    voice_actors = query.offset((page - 1) * items_per_page).limit(items_per_page).all() 
    
    return render_template('voice_actors.html', voice_actors=voice_actors, page=page, total_pages=total_pages, sort_by=sort_by, order=order)


@app.route('/search')
def search():
    query = request.args.get('q')
    
    # handle empty search 
    if not query:
        results = []
        return render_template('search_results.html', results=results, query=query)
    
    # split terms, query database for terms that match. don't compare case.
    terms = query.split()
    filters = []
    for term in terms:
        filters.append(Shows.title.ilike(f'%{term}%'))
        filters.append(Characters.name.ilike(f'%{term}%'))
        filters.append(Voice_Actors.name.ilike(f'%{term}%'))
    
    # joins ensure we get a reasonable number of results, otherwise we'd get a cartesian product (huge)
    results = (
        db.session.query(Shows, Characters, Voice_Actors)
        .join(CharactersShows, CharactersShows.show_id == Shows.id)  
        .join(Characters, CharactersShows.character_id == Characters.id)  
        .join(Voice_Actors, Characters.voiced_by_id == Voice_Actors.id)  
        .filter(or_(*filters))
        .distinct()
        .all()
    )
    
    shows = set()
    characters = set()
    voice_actors = set()
    empty = 0

    for row in results:
        shows.add(row[0]) 
        characters.add(row[1])
        voice_actors.add(row[2])

    shows = list(shows)
    characters = list(characters)
    voice_actors = list(voice_actors)
    if not shows:
        empty = 1

    return render_template('search_results.html', shows = shows, characters = characters, voice_actors = voice_actors, empty = empty, query=query)

@app.route('/api/characters/', methods=['GET'])
def get_characters():
    id = request.args.get('id')
    if id:
        id = int(id)
    name = request.args.get('name')
    voiced_by_id = request.args.get('voiced_by_id')
    if voiced_by_id:
        voiced_by_id = int(voiced_by_id)
    image = request.args.get('image')
    about = request.args.get('about')

    query = Characters.query

    if id:
        query = query.filter(Characters.id == id)
    if name:
        query = query.filter(Characters.name == name)
    if voiced_by_id:
        query = query.filter(Characters.voiced_by_id == voiced_by_id)
    if image:
        query = query.filter(Characters.image == image)
    if about:
        query = query.filter(Characters.about == about)

    data = [character.serialize() for character in query.all()]

    return jsonify(data)

@app.route('/api/shows/', methods=['GET'])
def get_shows():
    id = request.args.get('id')
    if id:
        id = int(id)
    title = request.args.get('title')
    image = request.args.get('image')
    episodes = request.args.get('episodes')
    if episodes:
        episodes = int(episodes)
    rating = request.args.get('rating')
    status = request.args.get('status')

    query = Shows.query

    if id:
        query = query.filter(Shows.id == id)
    if title:
        query = query.filter(Shows.title == title)
    if image:
        query = query.filter(Shows.image == image)
    if episodes:
        query = query.filter(Shows.episodes == episodes)
    if rating:
        query = query.filter(Shows.rating == rating)
    if status:
        query = query.filter(Shows.status == status)

    data = [show.serialize() for show in query.all()]

    return jsonify(data)

@app.route('/api/voices/', methods=['GET'])
def get_voice_actors():
    id = request.args.get('id')
    if id:
        id = int(id)
    name = request.args.get('name')
    birthday = request.args.get('birthday')
    image = request.args.get('image')
    about = request.args.get('about')

    query = Voice_Actors.query

    if id:
        query = query.filter(Voice_Actors.id == id)
    if name:
        query = query.filter(Voice_Actors.name == name)
    if birthday:
        query = query.filter(Voice_Actors.birthday == birthday)
    if image:
        query = query.filter(Voice_Actors.image == image)
    if about:
        query = query.filter(Voice_Actors.about == about)

    data = [voice_actor.serialize() for voice_actor in query.all()]

    return jsonify(data)

# @app.route('/api/character-shows/', methods=['GET'])
# def get_character_shows():
#     character_id = request.args.get('character_id')
#     show_id = request.args.get('show_id')

#     query = CharactersShows.query

#     if character_id:
#         query = query.filter(CharactersShows.character_id == character_id)
#     if show_id:
#         query = query.filter(CharactersShows.show_id == show_id)

#     data = [character_show.serialize() for character_show in query.all()]

#     return jsonify(data)


if __name__ == '__main__':
	app.debug = True
	app.run(host = '127.0.0.1', port = 5000)
