#!/usr/bin/env python3

from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from markupsafe import Markup
import os
import re

# initializing Flask app 
app = Flask(__name__)


# custom Flask filter
@app.template_filter()
def highlight_search_term(text, search_term):
    pattern = re.compile(re.escape(search_term), re.IGNORECASE)
    highlighted_text = pattern.sub(r'<mark>\g<0></mark>', str(text))
    return Markup(highlighted_text)


app.app_context().push()
# Change this accordingly 
USER = "postgres"
PASSWORD = "password"
PUBLIC_IP_ADDRESS = "localhost:5432"
DBNAME = "anibase"

# Make these command line arguments that provide when you deploy the app
# or use other options like connecting directly from App Engine


# Configuration 
app.config['SQLALCHEMY_DATABASE_URI'] = \
    os.environ.get("DB_STRING", f'postgresql://{USER}:{PASSWORD}@{PUBLIC_IP_ADDRESS}/{DBNAME}')
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = True  # To suppress a warning message

# initialize database
db = SQLAlchemy(app)


class CharactersShows(db.Model):
    """
    CharactersShows class has two attrbiutes 
    character_id
    show_id
    """
    __tablename__ = 'Character-Shows'

    character_id = db.Column(db.Integer, db.ForeignKey('Characters.id'), primary_key=True)
    show_id = db.Column(db.Integer, db.ForeignKey('Shows.id'), primary_key=True)

    def serialize(self):
        """
       returns a dictionary
       """
        return {
            'character_id': self.character_id,
            'show_id': self.show_id
        }


charactershows = CharactersShows.__table__  # Reference the table object from the model


class Shows(db.Model):
    """
    Shows class has six attributes
    id
    title
    image
    episodes
    rating
    status
    """
    __tablename__ = 'Shows'

    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.String(80), nullable=False)
    image = db.Column(db.String(80), nullable=False)
    episodes = db.Column(db.Integer, nullable=False)
    rating = db.Column(db.String(80), nullable=False)
    status = db.Column(db.String(80), nullable=False)

    charshows = db.relationship('Characters', secondary=charactershows, back_populates='charshows')

    # ------------
    # serialize
    # ------------
    def serialize(self):
        """
       returns a dictionary
       """
        return {
            'id': self.id,
            'title': self.title,
            'image': self.image,
            'episodes': self.episodes,
            'rating': self.rating,
            'status': self.status
        }


class Characters(db.Model):
    """
    Characters class has five attributes 
    id
    name
    voiced_by_id
    image
    about
    """
    __tablename__ = 'Characters'

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(80), nullable=False)
    voiced_by_id = db.Column(db.Integer, db.ForeignKey('Voice-Actors.id'), nullable=True)
    image = db.Column(db.String(80), nullable=False)
    about = db.Column(db.Text(), nullable=True)
    charshows = db.relationship('Shows', secondary=charactershows, back_populates='charshows')
    voice = db.relationship('Voice_Actors', back_populates='characters')

    # ------------
    # serialize
    # ------------
    def serialize(self):
        """
       returns a dictionary
       """
        return {
            'id': self.id,
            'name': self.name,
            'voiced_by_id': self.voiced_by_id,
            'image': self.image,
            'about': self.about
        }


class Voice_Actors(db.Model):
    """
    Voice_Actors class has five attributes 
    title
    id
    birthday
    image
    about
    """
    __tablename__ = 'Voice-Actors'

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(80), nullable=True)
    birthday = db.Column(db.String(10), nullable=True)
    image = db.Column(db.String(80), nullable=True)
    about = db.Column(db.Text(), nullable=True)

    characters = db.relationship('Characters', back_populates='voice')

    # ------------
    # serialize
    # ------------
    def serialize(self):
        """
       returns a dictionary
       """
        return {
            'id': self.id,
            'name': self.name,
            'birthday': self.birthday,
            'image': self.image,
            'about': self.about
        }


db.create_all()
